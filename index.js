//import express from express;
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const port = 3000;

// parse application/json
app.use(bodyParser.json())

// Ruta para peticiones

// HTTP: get, post, put ...

app.post('/sumar', function (peticion, respuesta){

    let number1 = peticion.body.number1;
    let number2 = peticion.body.number2;
    
    if(isNaN(number1) || isNaN(number2)){
        respuesta.sendStatus(400).send('Numeros inválidos');
    } else {
        let suma = parseFloat(number1) + parseFloat(number2);
        respuesta.send({ resultado : suma });
    }
    
});

// "prender" un servidor, Escuchar peticiones 

app.listen(port, () => {
  console.log(`Servidor activo en http://localhost:${port}`)
})